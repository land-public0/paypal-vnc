<?php


include '../config.php';
include 'paypal.class.php';


if( api_user == '' ){
    echo '0.00 N/A';
    die();
}


# else

$paypal = new Paypal( api_user, api_pass, api_signature );
$r = $paypal->call('GetBalance', array('RETURNALLCURRENCIES' => 1));


// $arrContextOptions = array( "ssl"=>array( "verify_peer"=>false, "verify_peer_name"=>false ) );
// $ex_rate = file_get_contents( 'http://api.exchangeratesapi.io/latest', false, stream_context_create($arrContextOptions) );
$ex_rate = '{"base":"EUR","rates":{"BGN":1.9558,"NZD":1.6694,"ILS":4.0255,"RUB":73.268,"CAD":1.5042,"USD":1.1246,"PHP":58.633,"CHF":1.1245,"ZAR":15.8562,"AUD":1.5823,"JPY":125.36,"TRY":6.3781,"HKD":8.8247,"MYR":4.6123,"THB":35.942,"HRK":7.4318,"NOK":9.6305,"IDR":15927.71,"DKK":7.4651,"CZK":25.634,"HUF":321.54,"GBP":0.86183,"MXN":21.439,"KRW":1288.11,"ISK":133.6,"SGD":1.5243,"BRL":4.3558,"PLN":4.2887,"INR":78.3215,"RON":4.7512,"CNY":7.5561,"SEK":10.4325},"date":"2019-04-08"}';
$ex_rate = json_decode($ex_rate, true);
$ex_rate = $ex_rate['rates'];
$ex_rate['EUR'] = 1;


for( $i=0; $i<=20; $i++ ){
    
    if(! isset($r['L_CURRENCYCODE'.$i]) ){
        break;
    }
    
    if( $ex_rate_this = $ex_rate[ $r['L_CURRENCYCODE'.$i] ] ){
        $sum+= floatval( $r['L_AMT'.$i] / $ex_rate_this );
    }
    
}

$sum = number_format($sum, 2, '.', '');

if( isset($_GET['xml']) ){
    header ("Content-Type:text/xml");
    echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
    echo "<items>\n";
    echo "<balance>". $sum ."</balance>\n";
    echo "<currency>EUR</currency>\n";
    echo "<date>". gmdate("H:i:s") ."</date>\n";
    echo "</items>\n";

} else if( $r['L_CURRENCYCODE0'] == '' ) {
    echo $r['L_LONGMESSAGE0'];
    
} else {
    echo $sum." EUR";
}





