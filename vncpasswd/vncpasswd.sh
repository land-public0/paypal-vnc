
if_any_vnc=$(lsof -Pni | grep Xvnc | grep -v LISTEN | grep :590 | wc -l)

if [ "$if_any_vnc" == "0" ]; then
    
    rand1=$(md5sum /etc/passwd | awk {'print $1'})
    rand=$(echo -n $rand1"unix"$(date +%s) | md5sum)
    rand=${rand:2:8}
    
    su root -c "{ echo '"$rand"'; echo '"$rand"'; echo n; } | vncpasswd" >/dev/null 2>&1
    
    echo "pass: "$rand

else
    echo "in_use"
fi

